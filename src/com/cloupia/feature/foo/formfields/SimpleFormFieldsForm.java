package com.cloupia.feature.foo.formfields;



import com.cloupia.feature.foo.lovs.SimpleLovProvider;
import com.cloupia.feature.foo.lovs.SimpleTabularProvider;
import com.cloupia.model.cIM.FormFieldDefinition;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;
import com.cloupia.service.cIM.inframgr.forms.wizard.HtmlPopupStyles;

/**
 * A super simple example of a form object.
 *
 */
public class SimpleFormFieldsForm {
	
	@FormField(label = "Name", help = "Name")
 	private String name;

    @FormField(label = "FIELD_TYPE_EMBEDDED_LOV", help = "Value", type = FormFieldDefinition.FIELD_TYPE_EMBEDDED_LOV, lovProvider = SimpleLovProvider.SIMPLE_LOV_PROVIDER)
    private String value;
    
    @FormField(label = "FIELD_TYPE_NUMBER", type = FormFieldDefinition.FIELD_TYPE_NUMBER, minValue = 1 , maxValue = 65535)
    private int number;
    
    @FormField(label = "FIELD_TYPE_BOOLEAN", type = FormFieldDefinition.FIELD_TYPE_BOOLEAN)
    private boolean boolType;
    
    @FormField(label = "FIELD_TYPE_PASSWORD", type = FormFieldDefinition.FIELD_TYPE_PASSWORD)
    private String password;
        
    @FormField(label = "FIELD_TYPE_DATE", type = FormFieldDefinition.FIELD_TYPE_DATE)
    private long dateLong;
    
    @FormField(label = "FIELD_TYPE_DATE_TIME", type = FormFieldDefinition.FIELD_TYPE_DATE_TIME)
    private long dateTime;
    
    @FormField(label = "FIELD_TYPE_MULTI_SELECT_LIST", type = FormFieldDefinition.FIELD_TYPE_MULTI_SELECT_LIST, lovProvider = SimpleLovProvider.SIMPLE_LOV_PROVIDER)
    private String listValue;
    
    @FormField(label = "FIELD_TYPE_FILE_UPLOAD", type = FormFieldDefinition.FIELD_TYPE_FILE_UPLOAD)
    private String uploadFileName;
    
    @FormField(label = "FIELD_TYPE_TABULAR_POPUP", type = FormFieldDefinition.FIELD_TYPE_TABULAR_POPUP, table = SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER)
    private String tabularPopup;
    
    //for tabular types, multiline = true gives you a string[] but multiline = false gives you just a String
    @FormField(label = "FIELD_TYPE_TABULAR", type = FormFieldDefinition.FIELD_TYPE_TABULAR, table = SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER, multiline = true)
    private String[] plainTabularValues;
    
    
    
    
    /* FormFields
     * 
     */
    @FormField(type = FormFieldDefinition.FIELD_TYPE_LABEL, label = "FIELD_TYPE_LABEL", 
			htmlPopupTag = "http://www.cisco.com",
			htmlPopupLabel = "http://www.cisco.com", htmlPopupStyle = HtmlPopupStyles.CUSTOM_URL)
	private String dummyLink;
    
    @FormField(label = "FIELD_TYPE_EMBEDDED_LOV", help = "Protocol", type = FormFieldDefinition.FIELD_TYPE_EMBEDDED_LOV, validate = true, lov = {
			"http", "https" })
	private String protocol="http";
    
    @FormField(type = FormFieldDefinition.FIELD_TYPE_HTML_LABEL, label = "FIELD_TYPE_HTML_LABEL" , htmlPopupLabel = "<a href='http://www.cisco.com'>Cisco</a>")
	private String dummyLink2;
    
    @FormField(label = "FIELD_TYPE_EMBEDDED_LOV_RADIO", type = FormFieldDefinition.FIELD_TYPE_EMBEDDED_LOV_RADIO, mandatory = true, lov = {
    		"Mode 1", "Mode 2", "Mode 3"}, validate = true, group="FIELD_TYPE_EMBEDDED_LOV_RADIO")
	private String modeType = "Select Mode";
    
    
    @FormField(label = "FIELD_TYPE_HTML_TEXT", type = FormFieldDefinition.FIELD_TYPE_HTML_TEXT, editable = true, size=FormFieldDefinition.FIELD_SIZE_MEDIUM_SMALL)
	private String         	status = "<h1>FIELD_TYPE_HTML_TEXT</h1>";
    
    @FormField(label = "FIELD_TYPE_LABEL_WITH_SPACE", help = "Ordering of VNICs", type = FormFieldDefinition.FIELD_TYPE_LABEL_WITH_SPACE)
    private String vnicLabel;
    
    @FormField(label = "FIELD_TYPE_IMAGE_SELECT_LIST", type = FormFieldDefinition.FIELD_TYPE_IMAGE_SELECT_LIST, mandatory = false, editable = true)
    private String catalogIcon;
    
    @FormField(label = "FIELD_TYPE_BUTTON_PANEL", type = FormFieldDefinition.FIELD_TYPE_BUTTON_PANEL, lov = {"Discover Servers"},validate = true,  group = "UCSM/CIMC Common",mandatory = false)
    private  String                             discoverServers      = "Discover Servers";
    
      
    @FormField(label = "FIELD_TYPE_TEXT_LINE_NUMS", help = "Error Text to validate", mandatory = false, 
            multiline = true, maxLength = 8192, type = FormFieldDefinition.FIELD_TYPE_TEXT_LINE_NUMS, size = FormFieldDefinition.FIELD_SIZE_SMALL_LARGE)
    private String             message;
    
      
    @FormField(label = "", help = "Upload a file", mandatory = true, validate = true, type = FormFieldDefinition.FIELD_TYPE_LARGE_FILE_UPLOAD, annotation = "For module uploads only zip format are supported")
    private String          uploadFile;
    
    @FormField(label = "FIELD_TYPE_COLORPICKER", help = "Color", mandatory = true, validate = true, type = FormFieldDefinition.FIELD_TYPE_COLORPICKER)
	private String color;
    
    public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getDummyLink2() {
		return dummyLink2;
	}

	public void setDummyLink2(String dummyLink2) {
		this.dummyLink2 = dummyLink2;
	}

	public String getModeType() {
		return modeType;
	}

	public void setModeType(String modeType) {
		this.modeType = modeType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVnicLabel() {
		return vnicLabel;
	}

	public void setVnicLabel(String vnicLabel) {
		this.vnicLabel = vnicLabel;
	}

	public String getCatalogIcon() {
		return catalogIcon;
	}

	public void setCatalogIcon(String catalogIcon) {
		this.catalogIcon = catalogIcon;
	}

	public String getDiscoverServers() {
		return discoverServers;
	}

	public void setDiscoverServers(String discoverServers) {
		this.discoverServers = discoverServers;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(String uploadFile) {
		this.uploadFile = uploadFile;
	}
    
    
    

	public String getDummyLink() {
		return dummyLink;
	}

	public void setDummyLink(String dummyLink) {
		this.dummyLink = dummyLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean getBoolType() {
		return boolType;
	}

	public void setBoolType(boolean boolType) {
		this.boolType = boolType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getDateLong() {
		return dateLong;
	}

	public void setDateLong(long dateLong) {
		this.dateLong = dateLong;
	}

	public long getDateTime() {
		return dateTime;
	}

	public void setDateTime(long dateTime) {
		this.dateTime = dateTime;
	}

	public String getListValue() {
		return listValue;
	}

	public void setListValue(String listValue) {
		this.listValue = listValue;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getTabularPopup() {
		return tabularPopup;
	}

	public void setTabularPopup(String tabularPopup) {
		this.tabularPopup = tabularPopup;
	}

	public String[] getPlainTabularValues() {
		return plainTabularValues;
	}

	public void setPlainTabularValues(String[] plainTabularValues) {
		this.plainTabularValues = plainTabularValues;
	}

}

