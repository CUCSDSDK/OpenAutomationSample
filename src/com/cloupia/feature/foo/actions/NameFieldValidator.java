package com.cloupia.feature.foo.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;
import com.cloupia.service.cIM.inframgr.forms.wizard.Page;
import com.cloupia.service.cIM.inframgr.forms.wizard.fieldvalidators.FieldValidatorIf;
import com.cloupia.service.cIM.inframgr.forms.wizard.fieldvalidators.ValidateInput;

/**
 * This class shows implementations for a custom validator.
 * Apart from available input field validators, developer can 
 * define their own customized validator as below.
 *  
 *
 */
public class NameFieldValidator implements FieldValidatorIf {
	
	static Logger logger = Logger.getLogger(NameFieldValidator.class);
    String regex = "[\"%&'*+,./:;<=>?^|\\\\]";
    
    public NameFieldValidator()
    {
    }

    /**
     * The validate method provides implementation for validating 
     * the name field with certain characters.
     * 
     * @param page: instance of form page getting validated
     * @param fieldName: provides field name to be validated
     * @param valInputAnnotation: additional required parameters 
     * 							from validator input annotation(say minValue, maxValue, regex)
     * @param fieldDef: form field definition for the field
     * @param value: input value provided by user
     * 
     * @throws Exception
     */
    @Override
    public void validate(Page page, String fieldName, ValidateInput valInputAnnotation, FormField fieldDef, Object value)
            throws Exception
    {
        if (value != null && value instanceof String)
        {
        	String name = (String) value;
        	Pattern pat = Pattern.compile(regex);
        	Matcher matcher = pat.matcher(name);
   		 
   		 	if (matcher.find())
   		 	{
   		 		throw new Exception("Specified Name is not valid, \nFollowing special characters are not allowed \", %, &, ', *, +, ., /, \\, :, ;, <, =, >, ?, ^, | and ,");
   		 	}
        }
    }

}
