package com.cloupia.feature.foo.lovs;

import com.cloupia.service.cIM.inframgr.customactions.UserInputValidatorIf;

/**
 * 
 * @author Administrator
 * This class demonstrates how to provide validators for workflow input types
 *
 */
public class EmailIdValidator implements UserInputValidatorIf{
	
	/**
	 * Provides validator description
	 * @return descriptions
	 */
	@Override
	public String getValidatorDescription() {
		return "Validating email address..";
	}

	/**
	 * Validates provided email id
	 * @param emailId
	 * @throws Exception
	 */
	@Override
	public void validateUserInput(String emailId) throws Exception {
		
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		
		Boolean b = emailId.matches(EMAIL_REGEX);
		
		if(! b){
			throw new Exception("Enter a valid Email address.");
		}
	}

}
