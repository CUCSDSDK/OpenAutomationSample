package com.cloupia.feature.foo;

import org.apache.log4j.Logger;

import com.cloupia.feature.foo.accounts.AccountSystemTaskReport;
import com.cloupia.feature.foo.accounts.DummyAccount;
import com.cloupia.feature.foo.accounts.FooAccount;
import com.cloupia.feature.foo.accounts.FooAccountSampleReport;
import com.cloupia.feature.foo.accounts.forms.UsersForm;
import com.cloupia.feature.foo.accounts.forms.UsersFormValidator;
import com.cloupia.feature.foo.accounts.handler.FooTestConnectionHandler;
import com.cloupia.feature.foo.accounts.inventory.FooConvergedStackBuilder;
import com.cloupia.feature.foo.accounts.inventory.FooInventoryItemHandler;
import com.cloupia.feature.foo.accounts.inventory.FooInventoryListener;
import com.cloupia.feature.foo.actions.NameFieldValidator;
import com.cloupia.feature.foo.api.config.CreateDummyAccountWithUsersConfig;
import com.cloupia.feature.foo.api.config.DummyAccountConfig;
import com.cloupia.feature.foo.api.config.DummyAccountCreateConfig;
import com.cloupia.feature.foo.api.config.DummyAccountDeleteConfig;
import com.cloupia.feature.foo.api.config.DummyAccountSpecificConfig;
import com.cloupia.feature.foo.api.config.DummyAccountUpdateConfig;
import com.cloupia.feature.foo.api.tasks.CreateDummyAccountTask;
import com.cloupia.feature.foo.api.tasks.CreateOAAPITask;
import com.cloupia.feature.foo.api.tasks.DeleteOAAPITask;
import com.cloupia.feature.foo.api.tasks.UpdateOAAPITask;
import com.cloupia.feature.foo.constants.FooConstants;
import com.cloupia.feature.foo.drilldownreports.FooAccountSampleDrillDownReport;
import com.cloupia.feature.foo.dummyOne.reports.DummyOneSampleReport;
import com.cloupia.feature.foo.formfields.SimpleFormFieldsReport;
import com.cloupia.feature.foo.lovs.DummyAccountLOVProvider;
import com.cloupia.feature.foo.lovs.EmailIdValidator;
import com.cloupia.feature.foo.lovs.SimpleLovProvider;
import com.cloupia.feature.foo.lovs.SimpleTabularProvider;
import com.cloupia.feature.foo.menuProvider.DummyMenuProvider;
import com.cloupia.feature.foo.multiselecttabularreports.MultiSelectTabularReport;
import com.cloupia.feature.foo.resourceComputer.DummyVLANResourceComputer;
import com.cloupia.feature.foo.scheduledTasks.DummyScheduleTask;
import com.cloupia.feature.foo.tasks.CreateGroupTask;
import com.cloupia.feature.foo.tasks.DisableSNMPNexusTask;
import com.cloupia.feature.foo.tasks.EmailDatacentersTask;
import com.cloupia.feature.foo.tasks.EnableSNMPNexusTask;
import com.cloupia.feature.foo.tasks.FooTaskContextTask;
import com.cloupia.feature.foo.tasks.HelloWorldTask;
import com.cloupia.feature.foo.tasks.MultiSelectTabularTask;
import com.cloupia.feature.foo.tasks.RollbackHelloWorldTask;
import com.cloupia.feature.foo.triggers.MonitorDummyDeviceStatusParam;
import com.cloupia.feature.foo.triggers.MonitorDummyDeviceType;
import com.cloupia.lib.connector.ConfigItemDef;
import com.cloupia.lib.connector.account.AccountTypeEntry;
import com.cloupia.lib.connector.account.PhysicalAccountTypeManager;
import com.cloupia.model.cIM.FormFieldDefinition;
import com.cloupia.model.cIM.InfraAccountTypes;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.AbstractCloupiaModule;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.CustomFeatureRegistry;
import com.cloupia.service.cIM.inframgr.collector.controller.CollectorFactory;
import com.cloupia.service.cIM.inframgr.customactions.WorkflowInputFieldTypeDeclaration;
import com.cloupia.service.cIM.inframgr.customactions.WorkflowInputTypeRegistry;
import com.cloupia.service.cIM.inframgr.forms.wizard.fieldvalidators.FieldValidtorRegistry;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReport;
import com.cloupia.service.cIM.inframgr.thresholdmonitor.MonitoringTrigger;
import com.cloupia.service.cIM.inframgr.thresholdmonitor.MonitoringTriggerUtil;
import com.cloupia.service.cIM.tree.MoParser;
import com.cloupia.service.cIM.tree.MoPointer;
import com.cloupia.service.cIM.tree.WFTaskRestAdaptor;

public class FooModule extends AbstractCloupiaModule {
	
	private static Logger logger = Logger.getLogger(FooModule.class);
	 public static final String CATEGORY_OPEN_AUTOMATION_TASKS      = "Open Automation Tasks";

	@Override
	public AbstractTask[] getTasks() {
		AbstractTask task1 = new CreateGroupTask();
		AbstractTask task2 = new EmailDatacentersTask();
		AbstractTask task3 = new HelloWorldTask();
		AbstractTask task4 = new EnableSNMPNexusTask();
		AbstractTask task5 = new DisableSNMPNexusTask();
		AbstractTask task6 = new RollbackHelloWorldTask();
		AbstractTask task7 = new FooTaskContextTask();
		AbstractTask task8 = new MultiSelectTabularTask();
		AbstractTask task9 = new CreateOAAPITask();
		AbstractTask task10 = new UpdateOAAPITask();
		AbstractTask task11 = new DeleteOAAPITask();
		AbstractTask task12 = new CreateDummyAccountTask();
		AbstractTask[] tasks = new AbstractTask[12];
		tasks[0] = task1;
		tasks[1] = task2;
		tasks[2] = task3;
		tasks[3] = task4;
		tasks[4] = task5;
		tasks[5] = task6;
		tasks[6] = task7;
		tasks[7] = task8;
		tasks[8] = task9;
		tasks[9] = task10;
		tasks[10] = task11;
		tasks[11] = task12;
		return tasks;
	}

	/**
	 * Getcollectors is not required for the creating new account type.
	 * getCollectors method be deprecated for the new account type.
	 * 
	 */
	@Override
	public CollectorFactory[] getCollectors() {
		return null;
	}

	@Override
	public CloupiaReport[] getReports() {
		//this is where you register all your top level reports, i'm only registering the report
		//extending genericinfraaccountreport because all my other reports are actually drilldown
		//reports of that report
		FooAccountSampleDrillDownReport drilReport = new FooAccountSampleDrillDownReport("foo.drilldown.report", "Drill Down", DummyAccount.class);
		
		CloupiaReport[] reports = new CloupiaReport[6];		
		reports[0] = new DummyOneSampleReport();
		reports[1] = new FooAccountSampleReport();
		
		reports[2] = drilReport;
		reports[3] = new AccountSystemTaskReport();
		reports[4] = new MultiSelectTabularReport();
		reports[5] = new SimpleFormFieldsReport();
		return reports;
	}

	@Override
	public void onStart(CustomFeatureRegistry cfr) {
		//this is where you would register stuff like scheduled tasks or resource computers

		//when registering new resource types to limit, you need to provide an id to uniquely identify the resource,
		//a description of how that resource is computed, and an instance of the computer itself
		this.registerResourceLimiter(FooConstants.DUMMY_VLAN_RESOURCE_TYPE, FooConstants.DUMMY_VLAN_RESOURCE_DESC, 
				new DummyVLANResourceComputer());

		try {
			//this is where you should register LOV providers for use in SimpleDummyAction
			cfr.registerLovProviders(SimpleLovProvider.SIMPLE_LOV_PROVIDER, new SimpleLovProvider());
			
			//this is where you should register LOV providers for use
			cfr.registerLovProviders(DummyAccountLOVProvider.DUMMY_LOV_PROVIDER, new DummyAccountLOVProvider());
			
			//you need to provide a unique id for this tabular provider, along with the implementation class, and the
			//index of the selection and display columns, for most cases, you can blindly enter 0
			cfr.registerTabularField(SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER, SimpleTabularProvider.class, "0", "0");
			//this is where you should add your schedule tasks
			addScheduleTask(new DummyScheduleTask());
			
			//registering new report context for use in my dummy menu, good rule of thumb, always register your contexts
			//as early as possible, this way you won't run into any cases where the context does not exist yet and causes
			//an issue elsewhere in the code!
			ReportContextRegistry.getInstance().register(FooConstants.DUMMY_CONTEXT_ONE, FooConstants.DUMMY_CONTEXT_ONE_LABEL);

			//FooAccount 
			ReportContextRegistry.getInstance().register(FooConstants.INFRA_ACCOUNT_TYPE, FooConstants.INFRA_ACCOUNT_LABEL);
			
			//Foo Drill down REport 
			ReportContextRegistry.getInstance().register(FooConstants.FOO_ACCOUNT_DRILLDOWN_NAME, FooConstants.FOO_ACCOUNT_DRILLDOWN_LABEL);
			
			//register the left hand menu provider for the menu item i'm introducing
			DummyMenuProvider menuProvider = new DummyMenuProvider();
			menuProvider.registerWithProvider();

			WorkflowInputTypeRegistry.getInstance().addDeclaration(new WorkflowInputFieldTypeDeclaration(
					  "usersListType", "usersListType",
			 	       FormFieldDefinition.FIELD_TYPE_TABULAR_NO_CHECKBOX, null));
			 WorkflowInputFieldTypeDeclaration usersListType = WorkflowInputTypeRegistry.getInstance().getDeclaration("usersListType");
	            usersListType.setFormManagedPojo(UsersForm.class );
	            usersListType.setFormManagedPojoValidator(UsersFormValidator.class);
			
			//registering workflow input type. You can use the registered input provider with workflows
			cfr.registerWorkflowInputFieldType(FooConstants.SAMPLE_LOV_PROVIDER_TYPE, FooConstants.SAMPLE_LOV_PROVIDER_TYPE_LABEL, 
					FormFieldDefinition.FIELD_TYPE_EMBEDDED_LOV, SimpleLovProvider.SIMPLE_LOV_PROVIDER);
			cfr.registerWorkflowInputFieldType(FooConstants.SAMPLE_TABULAR_PROVIDER_TYPE, FooConstants.SAMPLE_TABULAR_PROVIDER_TYPE_LABEL, 
					FormFieldDefinition.FIELD_TYPE_TABULAR_POPUP, SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER);
						
			//registering workflow input type as multi select. You can use the registered input provider
			cfr.registerWorkflowInputFieldType(FooConstants.SAMPLE_MULTISELECT_TABULAR_PROVIDER_TYPE, FooConstants.SAMPLE_MULTISELECT_TABULAR_PROVIDER_LABEL,
					FormFieldDefinition.FIELD_TYPE_TABULAR, SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER, true, null);
			
			//registering workflow input type with input validator
			cfr.registerWorkflowInputFieldType(FooConstants.EMAIL_INPUT_TYPE, FooConstants.EMAIL_INPUT_LABEL,
					FormFieldDefinition.FIELD_TYPE_TEXT, null, false, new EmailIdValidator());
			
			//adding new monitoring trigger, note, these new trigger components utilize the dummy context one i've just registered
			//you have to make sure to register contexts before you execute this code, otherwise it won't work
	        MonitoringTrigger monTrigger = new MonitoringTrigger(new MonitorDummyDeviceType(), new MonitorDummyDeviceStatusParam());
	        MonitoringTriggerUtil.register(monTrigger);
	        
	        //Register form input validator
	        FieldValidtorRegistry.getInstance().register(FooConstants.CUSTOM_NAME_FIELD_VALIDATOR, new NameFieldValidator());

			//support for new Account Type
			createAccountType();
		} catch (Exception e) {
			logger.error("Foo Module error registering components.", e);
		}
		
	}
	
	
	/**
	 * Creating New Account Type
	 */
	private void createAccountType(){
		AccountTypeEntry entry=new AccountTypeEntry();
		// This is mandatory, hold the information for device credential details
		entry.setCredentialClass(FooAccount.class);
		
		// This is mandatory, type of the Account will be shown in GUI as drill
		// down box
		entry.setAccountType(FooConstants.INFRA_ACCOUNT_TYPE);
		
		// This is mandatory, label of the Account
		entry.setAccountLabel(FooConstants.INFRA_ACCOUNT_LABEL);
		
		// This is mandatory, specify the category of the account type ie.,
		// Network / Storage / //Compute
		entry.setCategory(InfraAccountTypes.CAT_STORAGE);
		
		//This is mandatory
		entry.setContextType(ReportContextRegistry.getInstance().getContextByName(FooConstants.INFRA_ACCOUNT_TYPE).getType());
		
		// This is mandatory, on which accounts either physical or virtual
		// account , new account //type belong to.
		entry.setAccountClass(AccountTypeEntry.PHYSICAL_ACCOUNT);
		// Optional , prefix of the task
		entry.setInventoryTaskPrefix("Open Automation Inventory Task");
		
		//Optional. Group inventory system tasks under this folder. 
		//By default it is grouped under General Tasks
		entry.setWorkflowTaskCategory("Foo Tasks");
		// Optional , collect the inventory frequency, whenever required you can
		// change the
		// inventory collection frequency, in mins.
		entry.setInventoryFrequencyInMins(15);
		// This is mandatory,under which pod type , the new account type is
		// applicable.
		entry.setPodTypes(new String[] { "FlexPod","FooPod" });
		
		
		// This is optional, dependents on the need of session for collecting
		// the inventory
		//entry.setConnectorSessionFactory(new FooSessionFactory());
		
		// This is mandatory, to test the connectivity of the new account. The
		// Handler should be of type PhysicalConnectivityTestHandler.
		entry.setTestConnectionHandler(new FooTestConnectionHandler());
		// This is mandatory, we can implement inventory listener according to
		// the account Type , collect the inventory details.
		entry.setInventoryListener(new FooInventoryListener());
		
		//This is mandatory , to show in the converged stack view
		entry.setConvergedStackComponentBuilder(new FooConvergedStackBuilder());
		
		//This is required to show up the details of the stack view in the GUI 
		//entry.setStackViewItemProvider(new FooStackViewProvider());
		
		// This is required credential.If the Credential Policy support is
		// required for this Account type then this is mandatory, can implement
		// credential check against the policyname.
		//entry.setCredentialParser(new FooAccountCredentialParser());
				try {

					// Adding inventory root
					registerInventoryObjects(entry);
					PhysicalAccountTypeManager.getInstance().addNewAccountType(entry);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			private void registerInventoryObjects(
					AccountTypeEntry fooRecoverPointAccountEntry) {
				ConfigItemDef fooRecoverPointStateInfo = fooRecoverPointAccountEntry
						.createInventoryRoot("foo.inventory.root",
								FooInventoryItemHandler.class); 
			}
			/*
			 * InstallMoPointer is required to register REST API in the UCSD platform.
			 * 
			 */
			@Override
			public void installMoPointer(MoParser parser) {
		    	try {		    		
		    		
		    		/*
		    		 * Rest Adaptor is used to handle the CRUD operations for the Resource. 
		    		 * we can extend the adaptor functionality by inheriting the WFTaskRestAdaptor. 
		    		 * We can override the methods createResource, updateResource, deleteResource and query according to the need
		    		 */
		    		WFTaskRestAdaptor restAdaptor = new WFTaskRestAdaptor();
		    		/*
		    		 * MoPointer is the placeholder to register the REST APIs.
		    		 * @param0 is to define the resource name. mandatory field.
		    		 * @param1 is to define for  the ResourceURL.mandatory field.
		    		 * @param2 is restAdaptor. mandatory field.
		    		 * @param3 is the resource config class. mandatory field.
		      		 * If we don't want to READ Operation for the api have to use below constructor.
		    		 * MoPointer(String name, String path, MoResourceListener moListener, Class moModel, boolean isMoPersistent ,boolean isReadAPISupported)
		    		 * mopointer IS successfully registered API then only we can see the API in REST API Browser.
		    		 */
		    		 
		            MoPointer p = new MoPointer("FooAccount", "FooAccount", restAdaptor, DummyAccountConfig.class);  
		            /*
		             * createOARestOperation is mandatory method to register REST APIs operations via Open Automation connector.
		             * @param0 defines Operation name. mandatory field.
		             * @Param1 is resource Handler name. mandatory field.Using this handler name we are handling for the REST API Operation.
		             * @param2 is resource config class. mandatory field.For a particular handler operation config is required.
		             */
		            p.createOARestOperation("Create_Sample_Account", DummyAccountCreateConfig.HANDLER_NAME, DummyAccountCreateConfig.class);
		            p.createOARestOperation("Delete_Sample_Account", DummyAccountDeleteConfig.HANDLER_NAME, DummyAccountDeleteConfig.class);
		            p.createOARestOperation("Update_Sample_Account", DummyAccountUpdateConfig.MODIFY_HANDLER_NAME, DummyAccountUpdateConfig.class);      
		            p.createOARestOperation("Controller_REST_API", DummyAccountSpecificConfig.HANDLER_NAME, DummyAccountSpecificConfig.class);
		            p.createOARestOperation("CreateAccountWithUsersConfig",CreateDummyAccountWithUsersConfig.HANDLER_NAME, CreateDummyAccountWithUsersConfig.class);
			           
		            /*
		             * catagory is for the rest API browser folder structor.
		             */
		            p.setCategory(FooConstants.REST_API_FOLDER_NAME);
		            /*
		             * Registered REST APIs intimated to the framework through the MoParser.It is mandatory to load any REST APis in in the framework.
		             */
		            parser.addMoPointer(p);
		            
		    	} catch (Exception e) {
		    		logger.error("Error installing OA Storage APIs " + e);
		    	}
		    }
	}
